package com.diary;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.controller.DiaryController;
import com.dbutil.DBconnection;
import com.model.Diary;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData.Item;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class MainActivity extends Activity {

	
	private static final int REQUEST_CAMERA = 0;

	private static final int SELECT_FILE = 0;

	private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 0;

	private List<Diary> diaryList = new ArrayList<Diary>();

	TextView tv1;
	TextView tv2,v3;
	ListView listView;
	Button createButton;
	String userChoosenTask;
	Button select_photo;
	
	
ImageView ivImage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		DBconnection.setContext(this.getApplicationContext());
		setContentView(R.layout.fragment_main);

		Create_button_onClick();

		selectImage();
		initialize();

		
		listView_touch_action();

	}

	
	public void listView_touch_action() {
		DiaryAdapter adapter = new DiaryAdapter(MainActivity.this,
				R.layout.listview_item, diaryList);
		listView = (ListView) findViewById(R.id.listView1);
		listView.setAdapter(adapter);
		
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Diary diary = diaryList.get(position);

				final Dialog d = new Dialog(MainActivity.this);
				d.setTitle("Date");
				d.setContentView(R.layout.detail);
				
				tv1 = (TextView) d.findViewById(R.id.detailxml_date);
				
				tv1.setText(new java.text.SimpleDateFormat(("MM-dd")).format(diary.getDate()));
			
				tv2 = (TextView) d.findViewById(R.id.detailxml_moon);
				tv2.setText(diary.getMood());
				d.show();
			}
		});
	}

	
	public void Create_button_onClick() {
		createButton = (Button) this.findViewById(R.id.frag_create_button);
		createButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final Dialog c = new Dialog(MainActivity.this);
				c.setTitle("Cteate Date");
				c.setContentView(R.layout.create_detail);
				
				c.show();
			}
		});
	}

	public void initialize() {
		DiaryController control = new DiaryController();
		Date da = new Date();
		control.insert(da, "good", "well", "diary1", "diary1 context", da);
		diaryList = control.getDiaryList();

	}

	private void selectImage() {
		select_photo= (Button) findViewById(R.id.btnSelectPhoto);
		final CharSequence[] items = { "Take Photo", "Choose from Library",
		"Cancel" };
		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		builder.setTitle("Add Photo!");
		builder.setItems(items, new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int item) {
		boolean result = false;
		if (items[item].equals("Take Photo")) {
		userChoosenTask="Take Photo";
		if(result)
		cameraIntent();
		} else if (items[item].equals("Choose from Library")) {
		userChoosenTask="Choose from Library";
		if(result)
		galleryIntent();
		} else if (items[item].equals("Cancel")) {
		dialog.dismiss();
		}
		}
		});
		builder.show();
		}
	private void cameraIntent()
	{
	Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	startActivityForResult(intent, REQUEST_CAMERA);
}
	
	private void galleryIntent()
	{
	Intent intent = new Intent();
	intent.setType("image/*");
	intent.setAction(Intent.ACTION_GET_CONTENT);//
	startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
	}
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		switch (requestCode) {
		case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
		if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
		if(userChoosenTask.equals("Take Photo"))
		cameraIntent();
		else if(userChoosenTask.equals("Choose from Library"))
		galleryIntent();
		} else {
		//code for deny
		}
		break;
		}
		}
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		  super.onActivityResult(requestCode, resultCode, data);
		  if (resultCode == Activity.RESULT_OK) {
		     if (requestCode == SELECT_FILE)
		        onSelectFromGalleryResult(data);
		     else if (requestCode == REQUEST_CAMERA)
		        onCaptureImageResult(data);
		  }
		}
		@SuppressWarnings("deprecation")
		private void onSelectFromGalleryResult(Intent data) {
		  Bitmap bm=null;
		  if (data != null) {
		     try {
		        bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
		     } catch (IOException e) {
		        e.printStackTrace();
		     }
		  }
		  ivImage.setImageBitmap(bm);
		}
		private void onCaptureImageResult(Intent data) {
			Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
			File destination = new File(Environment.getExternalStorageDirectory(),
			System.currentTimeMillis() + ".jpg");
			FileOutputStream fo;
			try {
			destination.createNewFile();
			fo = new FileOutputStream(destination);
			fo.write(bytes.toByteArray());
			fo.close();
			} catch (FileNotFoundException e) {
			e.printStackTrace();
			} catch (IOException e) {
			e.printStackTrace();
			}
			ivImage.setImageBitmap(thumbnail);
			}
	
}